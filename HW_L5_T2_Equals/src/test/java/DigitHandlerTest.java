import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture5.DigitHandler;

public class DigitHandlerTest {

    /**
     * Checking the successful execution DigitHandler#equals()
     */
    @Test
    public void equals_Test(){

        DigitHandler digitOne = new DigitHandler(4);
        DigitHandler digitTwo = new DigitHandler(4);
        DigitHandler digitThree = new DigitHandler(44);

        boolean expected = true;
        boolean actual =digitOne.equals(digitTwo);
        Assert.assertEquals(expected, actual);

        actual = digitTwo.equals(digitThree);
        Assert.assertNotEquals(expected, actual );

        actual = digitOne.equals(digitOne);
        Assert.assertEquals(expected, actual);

        DigitHandler digitFour = digitOne;
        actual = digitOne.equals(digitFour);
        Assert.assertEquals(expected, actual);

        actual = digitTwo.equals(null);
        Assert.assertNotEquals(expected, actual );

    }

    /**
     * Checking the successful execution DigitHandler#hashCode()
     */
    @Test
    public void hashCode_Test(){

        DigitHandler digitOne = new DigitHandler(4);
        DigitHandler digitTwo = new DigitHandler(4);
        DigitHandler digitThree = new DigitHandler(44);

        int expected = digitOne.hashCode();
        int actual = digitTwo.hashCode();
        Assert.assertEquals(expected, actual);

        actual = digitThree.hashCode();
        Assert.assertNotEquals(expected, actual);
    }
}
