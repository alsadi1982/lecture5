import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture5.Person;

public class PersonTest {

    /**
     *Checking the successful execution Person#equalse()
     */
    @Test
    public void equals_Test(){

        Person person1 = new Person(null, null, 46);
        Person person2 = new Person("Valera", "Moscow", 34);
        Person person3 = new Person("VALERA", "moscow", 34);

        boolean expected = true;
        boolean actual = person2.equals(person3);
        Assert.assertEquals(expected, actual);

        actual = person1.equals(person2);
        Assert.assertNotEquals(expected, actual);

    }

    /**
     *Checking the successful execution Person#hashCode()
     */
    @Test
    public void hashCode_Test() {

        Person person1 = new Person(null, null, 46);
        Person person2 = new Person("Valera", "Moscow", 34);
        Person person3 = new Person("VALERA", "moscow", 34);

        int expected = person2.hashCode();
        int actual = person3.hashCode();
        Assert.assertEquals(expected, actual);

        actual = person1.hashCode();
        Assert.assertNotEquals(expected, actual);
    }
}
