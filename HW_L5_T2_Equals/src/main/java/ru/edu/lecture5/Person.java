package ru.edu.lecture5;

import java.util.Objects;

public class Person {

    private String name;
    private String city;
    private int age;

    public Person(String name, String city, int age) {

        if (name == null){
            this.name = "UNNAMED";
        }else{
            this.name = name;
        }

        if (city == null){
            this.city = "UNKNOWN";
        }else{
            this.city = city;
        }
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null){
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }
        Person person = (Person) obj;
        return getAge() == person.getAge() && getName().equalsIgnoreCase(person.getName()) && getCity().equalsIgnoreCase(person.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName().toLowerCase(), getCity().toLowerCase(), getAge());
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}
