import org.junit.Assert;
import org.junit.Test;
import ru.edu.lacture5.CustomDigitComparator;

import java.util.Comparator;

public class CustomDigitComparatorTest {

    /**
     *Checking the successful execution CustomDigitComparator#compare(Integer lhs, Integer rhs)
     */
    @Test
    public void compare_Success_Test(){

        Comparator<Integer> cmpInteger = new CustomDigitComparator();
        int expected = 1;
        int actual = cmpInteger.compare(33, 40);
        Assert.assertEquals(expected, actual);

        expected = -1;
        actual = cmpInteger.compare(122, 45);
        Assert.assertEquals(expected, actual);

        expected = 0;
        actual = cmpInteger.compare(43, 11);
        Assert.assertEquals(expected, actual);

    }

    /**
     *Checking the unsuccessful execution CustomDigitComparator#compare(Integer lhs, Integer rhs)
        * Checking execution then one of argument is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void compare_Fail_Test(){

        Comparator<Integer> cmpInteger = new CustomDigitComparator();
        cmpInteger.compare(null, null);

    }
}
