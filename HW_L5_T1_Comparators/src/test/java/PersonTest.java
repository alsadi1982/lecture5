import org.junit.Assert;
import org.junit.Test;
import ru.edu.lecture5.Person;

public class PersonTest {

    /**
     *Checking the successful execution Person#compareTo(Person other)
     */
    @Test
    public void compareTo_Test(){

        Person person1 = new Person("Boris", "Novgorod", 23);
        Person person2 = new Person("Alex", "Novgorod", 24);

        int expected = 1;
        int actual = person1.compareTo(person2);
        Assert.assertEquals(expected, actual);

        expected = -1;
        actual = person2.compareTo(person1);
        Assert.assertEquals(expected, actual);

        Person person3 = new Person("Boris", "Novgorod", 23);
        Person person4 = new Person("boris", "novgorod", 24);

        expected = 0;
        actual = person3.compareTo(person4);
        Assert.assertEquals(expected, actual);
    }
}
