package ru.edu.lecture5;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        List<Person> list = new ArrayList<>();

        list.add(new Person("Alex", "Novgorod", 23));
        list.add(new Person("olga", "Moscow", 27));
        list.add(new Person("Boris", "OREL", 30));
        list.add(new Person("igor", "moscow", 21));
        list.add(new Person(null, "Kiev", 33));
        list.add(new Person("July", "Astana", 34));
        list.add(new Person("valera", null, 23));

        System.out.println("List before: " + list);

        list.sort(Person::compareTo);

        System.out.println("List after sort: " + list);
    }
}
