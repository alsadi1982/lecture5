package ru.edu.lecture5;

public class Person implements Comparable<Person>{

    private String name;
    private String city;
    private int age;

    public Person(String name, String city, int age) {

        if (name == null){
            this.name = "UNNAMED";
        }else{
            this.name = name;
        }

        if (city == null){
            this.city = "UNKNOWN";
        }else{
            this.city = city;
        }

        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int compareTo(Person other) {

        int result = this.getCity().compareToIgnoreCase(other.getCity());

        if ( result == 0 ){
            return this.getName().compareToIgnoreCase(other.getName());
        }

        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}
