package ru.edu.lacture5;

import java.util.Comparator;

public class CustomDigitComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer lhs, Integer rhs) {

        if (lhs == null || rhs == null){
            throw new IllegalArgumentException("ERROR: One of arguments is NULL!");
        }

        if (lhs%2 == 0 && rhs%2 != 0){
            return -1;
        }else if (lhs%2 != 0 && rhs%2 == 0){
            return 1;
        }else {
            return 0;
        }
    }
}
