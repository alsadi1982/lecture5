package ru.edu.lacture5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class App {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 1; i <= 10; i++){
            list.add(i);
        }


        System.out.println("List before compare: " + list);

        Comparator<Integer> cmpInteger = new CustomDigitComparator();

        list.sort(cmpInteger);

        System.out.println("List after compare: " + list);

        list.clear();

        for (int i = 1; i <= 10; i++){
            list.add(i);
        }

        list.add(3, null);
        list.add(5, null);
        System.out.println("List before compare: " + list);

        try {
            list.sort(cmpInteger);
        }catch (IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }

    }
}
